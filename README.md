This repository contains nmigen code to support Wishbone. It is based on MiSOC
Wishbone code. The code is only meant as an intermediate implementation
until a standard nmigen Wishbone implementation is available.
