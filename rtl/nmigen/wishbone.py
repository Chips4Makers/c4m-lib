from nmigen import *
from nmigen.lib.io import *
from nmigen.build import *
from nmigen.hdl.rec import *

__all__ = [
    "wishbone_layout", "WishboneRTLResource",
    "Wishbone", "WishboneMemory",
    "WishboneExtend",
]

def _wishbone_fields(
    data_width, address_width, sel_width, master, with_stall, registered_feedback, tags
):
    if master:
        dir_down = DIR_FANOUT
        dir_up = DIR_FANIN
    else:
        dir_down = DIR_FANIN
        dir_up = DIR_FANOUT

    fields = [
        ("addr", address_width, dir_down),
        ("dat_w", data_width, dir_down),
        ("dat_r", data_width, dir_up),
        ("we", 1, dir_down),
        ("cyc", 1, dir_down),
        ("stb", 1, dir_down),
        ("lock", 1, dir_down),
        ("ack", 1, dir_up),
        ("err", 1, dir_up),
        ("rty", 1, dir_up),
    ]
    if sel_width is not None:
        fields.append(("sel", sel_width, dir_down))
    if with_stall:
        fields.append(("stall", 1, dir_up))
    if registered_feedback:
        fields.extend((
            ("cti", 3, dir_down),
            ("bte", 2, dir_down),
        ))
    for name, width, dir in tags:
        if not dir in ["UP", "DOWN"]:
            raise ValueError("Wishbone tag dir has to be 'UP' or 'DOWN'; not {!r}".format(dir))
        fields.append((name, width, dir_up if dir == "UP" else dir_down))

    return fields

def wishbone_layout(
    data_width, address_width, sel_width=None,
    *, master=False, with_stall=True, registered_feedback=False, tags=()
):
    if sel_width is not None and (data_width % sel_width) != 0:
        raise ValueError("data_width has to be a multiple of sel_width")

    return Layout(_wishbone_fields(
        data_width, address_width, sel_width, master, with_stall, registered_feedback, tags
    ))


def WishboneResource(
    data_width, address_width, sel_width=None, number=0,
    *, master=False, with_stall=True, registered_feedback=False, tags=(), pin_prefix=None
):
    #TODO: Implement WishboneResource
    raise NotImplementedError()


def WishboneRTLResource(
    data_width, address_width, sel_width=None, number=0,
    *, master=False, with_stall=True, registered_feedback=False, tags=(), pin_prefix=None
):
    if sel_width is not None and (data_width % sel_width) != 0:
        raise ValueError("data_width has to be a multiple of sel_width")

    pin_prefix = "WB{}_".format(number) if pin_prefix is None else pin_prefix
    fields = _wishbone_fields(
        data_width, address_width, sel_width, master, with_stall, registered_feedback, tags
    )

    subsigs = []
    for name, width, dir in fields:
        if width == 1:
            pins = pin_prefix + name
        else:
            pins = " ".join(["{}{}_{}".format(pin_prefix, name, i) for i in range(width)])
        subsigs.append(Subsignal(name, Pins(pins, dir="i" if dir is DIR_FANIN else "o")))

    return Resource("wb", number, *subsigs)


class Wishbone(Record):
    @classmethod
    def request(cls, platform, number=0):
        wb_io = platform.request("wb", number)

        # Duplicate fields and pop fields to retain the tags
        fields = wb_io.fields.copy()

        field = fields.pop("cyc")
        assert(hasattr(field, "i") ^ hasattr(field, "o"))
        is_master_out = "o" in field.fields
        attr_down = "o" if is_master_out else "i"
        attr_up = "i" if is_master_out else "o"

        field = fields.pop("addr")
        address_width = len(field.fields[attr_down])

        field = fields.pop("dat_w")
        data_width = len(field.fields[attr_down])
        field = fields.pop("dat_r")
        assert(len(field.fields[attr_up]) == data_width)

        for n in ["we", "stb", "lock"]:
            field = fields.pop(n)
            assert(len(field.fields[attr_down]) == 1)
        for n in ["ack", "err", "rty"]:
            field = fields.pop(n)
            assert(len(field.fields[attr_up]) == 1)
            
        field = fields.pop("sel", None)
        sel_width = len(field.fields[attr_down]) if field is not None else None

        field = fields.pop("stall", None)
        with_stall = field is not None
        if with_stall:
            assert(len(field.fields[attr_up]) == 1)

        field = fields.pop("cti", None)
        registered_feedback = field is not None
        if registered_feedback:
            assert(len(field.fields[attr_down]) == 3)
            field = fields.pop("bte", None)
            assert(len(field.fields[attr_down]) == 2)
        else:
            assert("bte" not in fields)

        # The rest are tags
        tags = []
        for name, subsig in fields:
            assert(hasattr(subsig, attr_down) ^ hasattr(subsig, attr_up))
            dir = "DOWN" if hasattr(subsig, attr_down) else "UP"
            tags.append((name, n, dir))

        # If the Wishbone interface is master on the outside it will be slave on inside
        # and vice-versa.
        wb = cls(
            data_width, address_width, sel_width, master=not is_master_out, with_stall=with_stall,
            registered_feedback=registered_feedback, tags=tags
        )

        if is_master_out:
            statements = [
                wb_io.addr.o.eq(wb.addr),
                wb_io.dat_w.o.eq(wb.dat_w),
                wb.dat_r.eq(wb_io.dat_r.i),
                wb_io.we.o.eq(wb.we),
                wb_io.cyc.o.eq(wb.cyc),
                wb_io.stb.o.eq(wb.stb),
                wb_io.lock.o.eq(wb.lock),
                wb.ack.eq(wb_io.ack.i),
                wb.err.eq(wb_io.err.i),
                wb.rty.eq(wb_io.rty.i),
            ]
            if sel_width is not None:
                statements.append(wb_io.sel.o.eq(wb.sel))
            if with_stall:
                statements.append(wb.stall.eq(wb_io.stall.i))
            if registered_feedback:
                statemements += [
                    wb_io.cti.o.eq(wb.cti),
                    wb_io.bte.o.eq(wb.bte),
                ]
            for name, _, dir in tags:
                if dir == "DOWN": # This is opposite of output Wishbone interface!
                    statements.append(getattr(wb_io, name).o.eq(getattr(wb, name)))
                else:
                    statements.append(getattr(wb, name).eq(getattr(wb_io, name).i))
        else:
            statements = [
                wb.addr.eq(wb_io.addr.i),
                wb.dat_w.eq(wb_io.dat_w.i),
                wb_io.dat_r.o.eq(wb.dat_r),
                wb.we.eq(wb_io.we.i),
                wb.cyc.eq(wb_io.cyc.i),
                wb.stb.eq(wb_io.stb.i),
                wb.lock.eq(wb_io.lock.i),
                wb_io.ack.o.eq(wb.ack),
                wb_io.err.o.eq(wb.err),
                wb_io.rty.o.eq(wb.rty),
            ]
            if sel_width is not None:
                statements.append(wb.sel.eq(wb_io.sel.i))
            if with_stall:
                statements.append(wb_io.stall.o.eq(wb.stall))
            if registered_feedback:
                statemements += [
                    wb.cti.eq(wb_io.cti.i),
                    wb.bte.eq(wb_io.bte.i),
                ]
            for name, _, dir in tags:
                if dir == "UP":
                    statements.append(getattr(wb_io, name).o.eq(getattr(wb, name)))
                else:
                    statements.append(getattr(wb, name).eq(getattr(wb_io, name).i))

        return wb, statements


    def __init__(self,
        layout=None, *,
        data_width=None, address_width=None, sel_width=None,
        master=False, with_stall=True, registered_feedback=False, tags=(),
        name=None, fields=None, src_loc_at=0
    ):
        """Generate Wishbone interface

        layout argument is there to support .like() class method. It should be
        used in user code"""

        self._master = master

        if layout is None:
            assert data_width is not None and address_width is not None, \
                "data_width and address_width have to be specified for Wishbone()"
            assert fields is None, "fields argement only supported for .like() class method"
            layout = wishbone_layout(
                data_width, address_width, sel_width,
                master=master, with_stall=with_stall, registered_feedback=registered_feedback,
                tags=tags
            )

        super().__init__(layout, name=name, fields=fields, src_loc_at=src_loc_at + 1)


    @property
    def master(self):
        return self._master


class WishboneMemory(Elaboratable):
    def __init__(self, *, width, depth, sel_width=None, init=None, domain="sync", src_loc_at=0):
        if sel_width is not None:
            assert (width % sel_width) == 0

        # The interface is a Wishbone slave in pipelined mode.
        address_width = len("{:b}".format(depth))
        self.wishbone = Wishbone(data_width=width, address_width=address_width, sel_width=sel_width, src_loc_at=src_loc_at+1)
        self.mem = Memory(width=width, depth=depth, init=init)

        ##

        self._width = width
        self._depth = depth
        self._sel_width = sel_width
        self._domain = domain


    def elaborate(self, platform):
        m = Module()
        
        m.submodules.rp = rp = self.mem.read_port()
        m.submodules.wp = wp = self.mem.write_port(
            granularity=self._width // (self._sel_width or 1)
        )

        wb = self.wishbone
        iscycle = wb.cyc & wb.stb
        if self._sel_width is None:
            wp_en = iscycle & wb.we
        else:
            wp_en = Cat(iscycle & wb.we & s for s in wb.sel)
        m.d.comb += [
            rp.addr.eq(wb.addr),
            wb.dat_r.eq(rp.data),
            wp.addr.eq(wb.addr),
            wp.data.eq(wb.dat_w),
            wp.en.eq(wp_en),
        ]
        # Always ack
        m.d[self._domain] += wb.ack.eq(iscycle)

        return m


class WishboneSRAM(Elaboratable):
    def __init__(self, data_width, address_width, sel_width=None):
        we_width = sel_width if sel_width is not None else 1
        
        self.wishbone = Wishbone(data_width=data_width, address_width=address_width, sel_width=sel_width)

        self.ce = Signal()
        self.addr = Signal(address_width)
        self.d = Pin(data_width, dir="io")
        self.we = Signal(we_width)


    def elaborate(self, platform):
        wb = self.wishbone
        has_sel = hasattr(wb, "sel")

        m = Module()

        m.d.comb += [
            self.ce.eq(wb.cyc & wb.stb),
            self.addr.eq(wb.addr),
            wb.dat_r.eq(self.d.i),
            self.d.o.eq(wb.dat_w),
            self.d.oe.eq(1),
        ]
        if not has_sel:
            m.d.comb += self.we.eq(wb.cyc & wb.stb & wb.we)
        else:
            m.d.comb += self.we.eq(Cat(wb.cyc & wb.stb & wb.we & sel for sel in wb.sel))

        return m


class WishboneExtend(Elaboratable):
    """Extends the address width of a wishbone interface with possibility to mirror regions
    
    Parameters
    ----------
    value: Const or int
        If value is a Const the address will be extened with the width of Const and
        wishbone will only be access if address starts with the value of Const
        If value is an integer it is the new width of addr. The new value has to be
        higher then the interface on which Extend is called
    mirror: bool
        If True then the smaller region will be mirrored in the bigger address space
        otherwise it will be placed at begin of space. If a Const value is given
        mirror has to be kept to False.
    name: str
    src_loc_at: int

    Returns
    -------
    A Wishbone interface with the extended address width. This value needs to be
    added to ``submodules`` of the relevant Module.
    """
    def __init__(self, wishbone, value, *, width=None, mirror=False, name=None, src_loc_at=0):
        assert not wishbone.master, "Only slave wishbone supported at the moment"
        self._slave = wishbone

        if isinstance(value, Const):
            assert mirror == False, "Mirroring with a Const extension does not make sense"
            width = len(wishbone.addr) + len(value)
        elif type(value) == int:
            width = value
            # If not mirroring change value to Const(0) so elaborate always needs to mirror
            # if value is of type int
            if not mirror:
                value = Const(0, width - len(wishbone.addr))
        self._value = value

        if name is None:
            name = "{}_ext{}".format(wishbone.name, width)

        # Create new Wishbone and extend address width
        fields = [
            (n, s, d) if n != "addr" else (n, Shape(width, s.signed), d)
            for n, (s, d) in wishbone.layout.fields.items()
        ]
        self.wishbone = wb = wishbone.__class__(Layout(fields), name=name, src_loc_at=1 + src_loc_at)


    def elaborate(self, platform):
        wb = self.wishbone

        m = Module()

        other_awidth = len(self._slave.addr)

        if isinstance(self._value, Const):
            selected = Signal(reset_less=True)
            m.d.comb += selected.eq(self._value == wb.addr[other_awidth:])
        else:
            # When mirrored, slave is always selected
            selected = Const(1)

        for name, (_, direction) in wb.layout.fields.items():
            if name == "addr":
                m.d.comb += self._slave.addr.eq(wb.addr[:other_awidth])
            elif name in ("cyc", "stb"):
                m.d.comb += getattr(self._slave, name).eq(selected & getattr(wb, name))
            else:
                if direction == DIR_FANIN:
                    m.d.comb += getattr(self._slave, name).eq(getattr(wb, name))
                elif direction == DIR_FANOUT:
                    with m.If(selected):
                        m.d.comb += getattr(wb, name).eq(getattr(self._slave, name))

        return m
